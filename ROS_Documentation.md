**About ROS (Melodic) :**

**To switch from a ros-distribution to another if you have several of them on your computer :**
```bash
source /opt/ros/<rosdistrib>/setup.bash
```

**Create your workspace :**
```bash
mkdir "nomduws"/src
cd 'nomduws'/
catkin_make (--source mysrc for a remote file)(-DPYTHON_EXECUTABLE=/usr/bin/pyhton3 if you want to specify python3)
source devel/setup.bash
```

**Find an existing package in your workspace :**
```bash
rospack find <pkg>
```

**Compute the content of a directory :**
```bash
rosls <pkg>
```

**To go to a package root directory :**
```bash
roscd <pkg>
roscd logs pour se rendre où sont stockés les logs
```

**Create a package with the corresponding dependances**
```bash
catkin_create_pkg <pkg> [depend1] ... [depend n]
```

**Launch ros (required if you want to run a node) :**

```bash
ros core
```

**Compute active nodes :**

```bash
rosnode list
```
**Run the node n of the package p**

```bash
rosrun <p> <n>
```

**Topics**

```bash
rostopic -h affiche les commandes possibles
rostopic echo <topic> affiche les infos publiées sur le topic
rostopic type <topic> affiche le type du message publié
rostopic show <type> affiche les détails du type
rostopic pub (-r f publie à une fréquence f / -1 publie une fois) [topic] [msg_type] [args] (-- indique au parser de ne pas interpreter les - comme des options) [args]
#  --> ex : $ rostopic pub /turtle1/cmd_vel geometry_msgs/Twist -r 1 -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, -1.8]'
```
**rosparam ...**

```bash
list                            # cumpute parameters if roscore is running
set [param_name] [args]      # assign args the variable param_name
get [param_name]               # compute the parameter value - \\ to compute all
dump/load [filename] ([namespace]) # save/loads data in a file "filename" in a directory namespace
```

**Run a list of nodes defined in a launchfile :**
```bash
roslaunch [package] \[filename.launch]
```

**Edit a ros file :**
```bash
rosed [pakg_name] [filename]
```

**/!\ Packages to install :**

**rqt_graph (dynamic graph of what happens when ROS is running)**
* **installation** 

```bash
sudo apt-get install ros-<distro>-rqt
sudo apt-get install ros-<distro>-rqt-common-plugins
```
*  **To run it** 

```bash
rosrun rqt_graph rqt_graph
```

**rqt_plot (installé avec ros)**
* **To run it**

```bash
rosrun rqt_plot rqt_plot
```

**rqt-console**
* **Installation**

```bash
sudo apt-get install ros-<distro>-rqt ros-<distro>-rqt-common-plugins ros-<distro>-turtlesim
```

* **To run it**

```bash
rosrun rqt_console rqt_console (affiche les logs)
rosrun rqt_logger_level rqt_logger_level (choisit le niveau de logs à afficher)
```