echo "Detection-Program is running..."

export PYTHONPATH="/opt/movidius/caffe/python:${PYTHONPATH}"

# DETECTION with VIDEO
if [ $1 = "-video" ]
then
	#mvNCCompile prototxt/yolo_tiny_deploy.prototxt -w weights/yolo_tiny.caffemodel -s 12
	python3 py_examples/object_detection_app.py
	
fi

# DETECTION with SIMPLE IMAGE
if [ $1 = "-image" ]
then
	python3 py_examples/yolo_example.py images/"$2".jpg
fi
