**Fiche de suivi de projet : Intégration d'Intel Movidius ou MAix BiT à RobAIR**



**Semaine 1 : 27 Janvier - 2 Février**
*   Recherche de documentation portant sur I’Intel MOVIDIUS et le MAix BiT
*   Découverte de ROS / Installation de ROS
*   Étude d’exemples d’utilisation de ROS et de l’Intel MOVIDIUS



**Semaine 2 : 3 Février - 9 Février**
*   Rencontre avec M.Lemasson au FabLab avec prise de rendez-vous afin de clarifier les attentes du projet
*   Rédaction d'une ébauche de cahier des charges 


**Semaine 3 : 10 Février - 16 Février**
*   Suite au rdv avec M. Lemasson nous avons maintenant une idée plus précise des tâches à réaliser
*   Recherches concernant Docker, ROS Melodic et Gazebo, le but étant d'avoir un simulateur fonctionnel pour tester notre robot depuis l'ordinateur
*   PROBLEME : Comment installer le turtlebot sur ROS Melodic ?? OU Installer Ubuntu 14.04 + ROS + TurtleBot sur un Docker exécutable ??


**Semaine 4 : 17 Février - 23 Février**
*   TO DO : 1. Récupérer un Rasberry PI 
            2. Utiliser une VM (ubuntu 16.04 ou 14.04)
            3. Faire tourner ROS
            4. Faire tourner l'intel MOVIDIUS
*   Problemes avec l'installation de docker avec ROS dessus
*   Bonne compréhension du projet, du cahier des charges mais impossibilité de démarrer due aux difficultés d'installation des différents outils..


**Semaine 5 : 24 Février - 1 Mars**
*   Installation de l'interface openVino permettant d'utiliser les Movidius
*   Nous essayons d'installer le plug-in ROS par dessus, mais rencontrons quelques difficultés
*   Installation de VirtualBox avec Ubuntu 16.04 


**Semaine 6 : 2 Mars - 8 Mars**
*   Essais d'utilisation du Movidius mais impossible de le rendre détectable auprès de la VM
*   Emprunt d'un Rasberry PI au FabLab


**Semaine 7 : 9 Mars - 15 Mars**
*   Installation de OpenVino et récupération d'exemples d'utilisation du Movidius sur un GitLab partagé 
*   Tests sur ROS (exemple tortue, etc)
*   Réalisation du fichier de documentation sur ROS
*   Installation de ncsdk1 sur la VM Linux 16.04
*   Installation du projet yoloNCS : contient un programme d'analyse d'image, détectant différents objets sur une image (https://github.com/gudovskiy/yoloNCS)


**Semaine 8 : 16 Mars - 22 Mars**
*   Demande effectuée sur Stack Overflow concernant l'erreur sur Docker
*   Début d'implémentation d'un noeud "démo" de ROS avec un publisher et un receiver utilsant un topic
*   Analyse des fichier C++ déjà existants du projets RobAir notamment dans [RobAir.cpp](https://github.com/fabMSTICLig/RobAIR/blob/master/arduino/libraries/robair/Robair.cpp)


**Semaine 9 : 23 Mars - 29 Mars**
*   Réécriture des tutoriels en Markdown
*   Modification de l'output du programme de reconnaissance d'image 
*   


**Semaine 10 : 30 Mars - 5 Avril**
* Installation d'une VM en 16.04 pour Baptiste
* Fin de la rédaction du tutoriel pour installer ros-kinetic
* Instauration d'un .gitignore pour éviter les données inutiles sur le dépot Git
* Modification le l'output du programme de reconnaissance d'objets, qui renvoie maintenant le couple [type détécté, position X, position Y, Hauteur, Largeur] lorqu'il détecte un objet sur l'image
* Installation de ROS Kinetic pour Rémy en utilisant la documentation créée par Baptiste


**Semaine 11 : 6 Avril - 12 Avril**
* Entretient avec Didier Donsez
* Envoi de mail à M. Lemasson afin d'avoir des précisions concernant un éventuel simulateur de RobAIR
* Recherche de l'outil adéquat pour simuler RobAir
* Recherches sur la mise en relation entre la partie Movidius et la partie ros


**Semaine 12 : 13 Avril - 19 Avril**
* Début de réalisation du fichier awk (But: faire suivre du regard le robot)
* Commencement du rapport de Projet
* Mise à jour de l'output du programme de reconnaissance
* Simulateur choisi : TurtleSim de ROS
* Difficulté principale : problème de variable PYTHONPATH différente pour les environnements Movidius et ROS


**Semaine 13 : 20 Avril - 26 Avril**
* Création de scripts bash pour configurer les environnements des 2 services
* Réalisation du fichier awk de lecture de l'output
* Mise en relation des 2 services à l'aide de scripts bash
* Difficultés sur le fonctionnement de ros et de Movidius en simultané
* Avancement positif du rapport


**Semaine Finale : 27 Avril - 30 Avril**
* Finalisation du rapport
* Nouvelle version plus efficace du suivi de personne



**A faire (après visio Didier Donsez)** 
* [bash readline process](https://unix.stackexchange.com/questions/117501/in-bash-script-how-to-capture-stdout-line-by-line) ou rostopic pub pour passer du movidius au ROS
* Suivi d'objet (toujours avoir un certain objet en visuel)
* Faire un package hors-RobAir

