**Before you start to use ROS, you will have to install it via this [tutorial](http://wiki.ros.org/kinetic/Installation)**
**Then, from catkin_ws repo, execute :**

`source /opt/ros/kinetic/setup.bash`

`catkin_make`

`source devel/setup.bash`

**To run a node, from catkin_ws :** 

`roscore`

and in an other console : `rosrun <package name > <node name>`