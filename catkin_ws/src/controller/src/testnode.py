#!/usr/bin/env python
# license removed for brevity
import rospy, time
# from std_msgs.msg import String
from geometry_msgs.msg import Twist
import numpy as np
from threading import Thread

class Listener(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.ang = 0.0

    def run(self):
        #collect angular velocity from ros parameter server 
        #every 0.5 second
        while True:
            if rospy.has_param('angular'):
                self.ang = rospy.get_param('angular')
            else:
                self.ang = 0.0
            time.sleep(0.5)


        

def talker():
    #Starts a new node
    rospy.init_node('sender', anonymous=True)
    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    #run the thread which collects the angular velocity 
    th = Listener()
    th.start()   
    vel_msg = Twist()
    rate = rospy.Rate(2) #Publish on the topic every 0.5 second
    while ((not rospy.is_shutdown())):
        vel_msg.linear.x = 0
        vel_msg.linear.y = 0
        vel_msg.linear.z = 0
        vel_msg.angular.x = 0
        vel_msg.angular.y = 0
        vel_msg.angular.z = th.ang
        pub.publish(vel_msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
