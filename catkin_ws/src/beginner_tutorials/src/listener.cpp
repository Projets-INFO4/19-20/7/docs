#include "ros/ros.h"
#include "std_msgs/String.h"

//Fonction qui sera appelée quand on reçoit un message
//Elle va écrire dans les logfiles (avec le niveau d'importance INFO ) qu'elle a reçun message 
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
    // passer en arguments argc et argv permet de supporter n'importe quel argument ROS quelque soit son type
    // Cela permet aussi de supporter les "name-remappings" faits en ligne de commande (c-a-d si j'ai bien compris les changements de noms d'objets ROS ?)
    ros::init(argc, argv, "listener");

    // Attribue le noeud à un objet (initialise l'objet) --> Le noeud sera appelé via n 
    ros::NodeHandle n;

    // Dit au au master qu'on veut s'abonner au topic 'chatter'.
    // ("chatter",500, chatterCallback) donne le nom du topic qu'on advertise et donne la qté max de message de son buffer
    // Enfin chatterCallback donne le nom de la fonction Callback à appeler à chaque message reçu
    // Node::Handle.suscribe() renvoie un ros::Suscriber qui se "unsuscribe" automatiquement si il est détruit
    ros::Subscriber sub = n.subscribe("chatter", 500, chatterCallback);

    //Checke si il y a des callbacks reçus
    ros::spin();
}

