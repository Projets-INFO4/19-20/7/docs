#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>

int main(int argc, char **argv){
    // Initialise ros et permet de donner le nom du noeud
    ros::init(argc, argv, "talker");

    // Attribue le noeud à un objet (initialise l'objet) --> Le noeud sera appelé via n
    ros::NodeHandle n;

    // Dit au au master qu'on va publier sur 'chatter'.
    // Il va prevenir toutes les nodes abonnées à 'chatter'
    // Node::Handle.advertise renvoie un ros::Publisher qui contient un publish() et se "unadvertise" si il y a un souci 
    // <std_msgs::String> indique le type de message envoyé
    // ("chatter",1000) donne le nom du topic qu'on advertise et donne la qté max de message de son buffer
    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 500);

    // Donne la fréquence à laquelle on va boucler et surveille la durée depuis le dernier "Rate::sleep()"
    // Ici on va tourner à une fréquence de 10 Hz
    ros::Rate loop_rate(10);

    //ros::ok() passe à false quand :
    // - Un Ctrl + C est effectué
    // - Le noeud est exclu du réseau (par un noeud portant le même nom)
    // - ros::shutdown() est appelé autre part dans l'application 
    // - Tous les objets noeuds (c-a-d de type ros::NodeHandle) sont détruits
    int cpt = 0;
    while (ros::ok()){
        std_msgs::String msg;

        std::stringstream ss;
        ss << "hello world " << cpt;
        msg.data = ss.str();
        cpt++;

        //ROS_INFO est comparable à un printf() que l'on fait pout un fichier avec des avantages supplémentaires
        //Il contient + de métadonnées comme le timestamp, et ajoute automatiquements les données envoyées au logfile
        //Ici il va donc écrire dans les log files avec le niveau d'"importancee" INFO (parmi DEBUG, INFO, WARN, ERROR et FATAL)
        ROS_INFO("%s", msg.data.c_str());

        //On appelle le publisher pour partager les données à tout noeud connecté(ici le topic 'chatter')
        chatter_pub.publish(msg);

        //Ici ça ne sert à rien mais spinOnce() permet d'appeler d'éventuels callbacks
        ros::spinOnce();

        //Enfin on dort en se servant du noeud ros::Rate pour nous réveiller à une fréquence de 10 HZ
        loop_rate.sleep();
    }
}