* When we want to run a docker image ros:kinetic with
<code> docker run --rm -it -e WORKSPACE_NAME=workspace -v $(pwd)/workspace:/root/workspace -w /root/workspace/src ros:mykinetic catkin_create_pkg beginner_tutorials std_msgs rospy </code> 

Console returns : <code> standard_init_linux.go:211: exec user process caused "exec format error" </code>

Question asked on [Stack Overflow](https://stackoverflow.com/questions/60725360/problem-when-running-docker-image-roskinetic-standard-init-linux-go211-exec)

Resolved by Mr. Richard. It was because of the docker image which had the wrong architecture (ARM instead of x86_64)