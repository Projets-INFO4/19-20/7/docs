Demo Eye-Track with Movidius & Turtlebot :
==

1 : You must have installed the Movidius and ROS environment on your Ubuntu 16.04 VM before testing this demo !
-


2 : Go to the yoloNCS folder :
-
```bash
$ cd docs/yoloNCS
```

3 : Use this script to run the TurtleBot AND the detection program in the same time :
-
```bash
$ bash main_run.sh
```
