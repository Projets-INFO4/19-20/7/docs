Installation guide of Eye-tracking program using the object-detection with the Movidius :
==

Step 1 : Install VirtualBox (if it's not already installed)
-
* Download the latest version on https://www.virtualbox.org/
* Download the attached "Extension Pack" (https://www.virtualbox.org/wiki/Downloads, see section named VirtualBox 6.1.4 Oracle VM VirtualBox Extension Pack​ ).

    
Step 2 : Download .iso file of Ubuntu 16.04 (1.5 Go)
-
* Go to http://releases.ubuntu.com/16.04/
* Download 64-bits release (or 32 bits depending on your laptop).


Step 3 : Create Ubuntu 16.04 Virtual Machine
-
* Click on « New », and name your VM as you wish, and choose Linux type.
* Choose the RAM size : at least 2048 MB, if you can put more, no problem !
* For the next 3 steps («Hard Disk», «Hard Disk file type», «Storage on physical hard disk») , choose the default checked boxes, and click on next.
* Memory Size : take at least 20 GB.
* Once the VM created, go to parameters :  
    --> In the Système / Processeur tab : choose the number of cores used available for the VM (At least 2, 4 would be the most optimized !).  
    --> In the Stockage tab : Click on the empty field under « Controller : IDE », then click on the icon to choose an .iso file in you computer system-file
and select Ubuntu 16.04 image.   
    --> In the USB tab : check USB 3.0 (xHCI).  
    --> Conclude the configurationby clicking on OK on the bottom-left.  
* Your VM is now configured !


Step 4 : Ubuntu 16.04 installation
-
* Launch your brand new VM, freshly configured. 
* Follow installation steps of Ubuntu :  
    --> For the « installation type» part, you have to check the box « Supprimer le disque et installer Ubuntu » (nothing will be supressed, everything is virtual !).  
* Wait for the end of installation (takes some time...)
* Once the installation is complete, VM is fonctional !


Step 5 :  NCSDK 1.0 installation
-
* Open a console in the VM.
* Install git command with :  
```bash
$ sudo apt install git
```

* Download the project from the following address :
```bash
$ git clone https://github.com/movidius/ncsdk.git
```

* Go to /ncsdk driectory : 
```bash
$ cd ncsdk/
```

* Type this command : 
```bash
$ make install #will take some time
```
* A message is supposed to tell you evrything has been successfully installed !

**IMPORTANT**  
Before doing the continuation, don't forget to configure USB ports to make the Movidius visible !  
To do that, go to  Périphériques tab of VirtualBox, in the USB section, then click on the "add a filter" item.  

* You will have to fill the ID Vendeur box with the following field :  **03e7** which corresponds to the Movidius.
* Then, after plugging the Movidius to an USB port, run the command : 
```bash
$ make examples #will take time too
```
* Once this is done, NCSDK installation is now complete !


Step 6 : Be able to run the image-detection program in the folder yoloNCS of our project
-
* Go to yoloNCS directory and create a directory named weights :
```bash
$ cd yoloNCS/
$ mkdir weights
```
* Place in this directory the following file, available on the following Drive : https://drive.google.com/file/d/0Bzy9LxvTYIgKNFEzOEdaZ3U0Nms/view?usp=sharing
* Run the following command :
```bash
$ mvNCCompile prototxt/yolo_tiny_deploy.prototxt -w weights/yolo_tiny.caffemodel -s 12
```
* If the compilation fail, verify your PYTHONPATH
* You should have this line into the .bashrc :
```bash
$ export PYTHONPATH="${PYTHONPATH}:/opt/movidius/caffe/python"
```

**IMPORTANT**   
Before running the program, verify that your webcam is detectable by the VM !  

* To run the video-capture program with TurtleBot :
```bash
$ bash main_run.sh
```

* The detection quality will depend on the webcam quality !
* The Turtle will follow object's movement
* The model is trained to detect the  following categories : **"aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train","tvmonitor"**
